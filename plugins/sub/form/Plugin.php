<?php namespace Sub\Form;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return[
            'Sub\Form\Components\ContactForm2'=>'contactform2'
        ];
    }

    public function registerSettings()
    {
    }
}
