<?php namespace Sub\Form\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Validator;

class ContactForm2 extends ComponentBase

{ 

    public function componentDetails(){
        return[

            'name' => 'Contact Form',
            'description' => 'Simple contact form'
        ];
    }

    public function onSend2 (){

        $validator = Validator::make(
            [
             
                'email' => Input::get('email')
            ],
            [
               
                'email' => 'required|email'
           

            ]
        );

        if($validator->fails()){

  return Redirect::back()->withErrors($validator);


        }else{


        $vars = [ 'email' =>  Input::get('email')];

        Mail::send('sub.form::mail.message', $vars, function($message) {
        
            $message->to('irvin.pineda024@gmail.com', 'irvin');
            $message->subject('New message from contact form');
        
        });
    
    }
    }


}