<?php namespace Activamkt\Contact;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return[
        'Activamkt\Contact\Components\ContactForm'=>'contactform'
    ];
    
    }

    public function registerSettings()
    {
    }
}
