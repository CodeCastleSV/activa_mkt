<?php namespace Activamkt\Contact\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Validator;

class ContactForm extends ComponentBase

{ 

    public function componentDetails(){
        return[

            'name' => 'Contact Form',
            'description' => 'Simple contact form'
        ];
    }

    public function onSend (){

        $validator = Validator::make(
            [
                'name' => Input::get('name'),
                'company' =>  Input::get('company'),
                'phone' =>  Input::get('phone'),
                'select' =>  Input::get('select'),
                'email' => Input::get('email')
            ],
            [
                'name' => 'required',
                'email' => 'required|email',
                'company' => 'max:191',
                'phone' => 'numeric',
                'select' => 'required',

            ]
        );

        if($validator->fails()){

  return Redirect::back()->withErrors($validator);


        }else{


        $vars = ['name' => Input::get('name'), 'email' =>  Input::get('email'),'company' =>  Input::get('company'), 'phone' =>  Input::get('phone'), 'select' =>  Input::get('select')];

        Mail::send('activamkt.contact::mail.message', $vars, function($message) {
        
            $message->to('irvin.pineda024@gmail.com', 'irvin');
            $message->subject('New message from contact form');
        
        });
    
    }
    }


}