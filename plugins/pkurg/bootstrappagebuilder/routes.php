<?php

use Cms\Classes\Content;
use Cms\Classes\Page;
use Cms\Classes\Partial;
use Cms\Classes\Theme;
use KubAT\PhpSimple\HtmlDomParser;
use Pkurg\BootstrapPageBuilder\Models\Settings;
use RainLab\Pages\Classes\Page as StaticPage;

Route::post('pagebuilderuploader', 'Pkurg\BootstrapPageBuilder\Controllers\BuilderUploader@uploadFiles');

Route::get('b4bulder.css', function () {

    if (Input::has('type') and Input::has('file')) {
        //

        $type = Input::get('type');
        $file = Input::get('file');

        if ($type == 'partial') {
            $template = Partial::load(Theme::getActiveTheme(), $file);
            if (!isset($template->markup)) {
                return;
            }
            $html = $template->markup;

            //get styles
            $html = HtmlDomParser::str_get_html($html);

            if ($html) {
                $styles = '';
                foreach ($html->find('style') as $style) {
                    $styles = $styles . $style->innertext;
                }

                return response($styles, 200)
                    ->header('Content-Type', 'text/css');
            }
        }

        if ($type == 'content') {
            $template = Content::load(Theme::getActiveTheme(), $file);
            if (!isset($template->markup)) {
                return;
            }
            $html = $template->markup;

            //get styles
            $html = HtmlDomParser::str_get_html($html);
            if ($html) {
                $styles = '';
                foreach ($html->find('style') as $style) {
                    $styles = $styles . $style->innertext;
                }

                return response($styles, 200)
                    ->header('Content-Type', 'text/css');
            }

        }
    }

    if (Input::has('page')) {

        $page = Input::get('page');

        $template = Page::load(Theme::getActiveTheme(), $page);

        if ($template) {

            if (array_key_exists('page_builder_styles', $template->viewBag)) {

                return response($template->viewBag['page_builder_styles'], 200)
                    ->header('Content-Type', 'text/css');
            }
        }

    }

    if (Settings::get('show_static_page')) {
        if (Input::has('static-page')) {

            $page = Input::get('static-page');

            $template = StaticPage::load(Theme::getActiveTheme(), $page);

            if ($template) {

                if (array_key_exists('page_builder_styles', $template->viewBag)) {

                    return response($template->viewBag['page_builder_styles'], 200)
                        ->header('Content-Type', 'text/css');
                }
            }

        }
    }

});

Route::get('b4bulder.js', function () {

    if (Input::has('page')) {

        $page = Input::get('page');
        $template = Page::load(Theme::getActiveTheme(), $page);

        if ($template) {

            if (array_key_exists('page_builder_scripts', $template->viewBag)) {

                $s = $template->viewBag['page_builder_scripts'];

                $html = HtmlDomParser::str_get_html($s);

                if ($html) {
                    $scripts = '';
                    foreach ($html->find('script') as $script) {

                        if ($script->innertext != '') {

                            $scripts = $scripts . $script->innertext;

                        }

                    }

                }

                return response($scripts, 200)->header('Content-Type', 'text/javascript');
            }
        }

    }

    if (Settings::get('show_static_page')) {
        if (Input::has('static-page')) {

            $page = Input::get('static-page');
            $template = StaticPage::load(Theme::getActiveTheme(), $page);

            if ($template) {

                if (array_key_exists('page_builder_scripts', $template->viewBag)) {

                    $s = $template->viewBag['page_builder_scripts'];

                    $html = HtmlDomParser::str_get_html($s);

                    if ($html) {
                        $scripts = '';
                        foreach ($html->find('script') as $script) {

                            if ($script->innertext != '') {

                                $scripts = $scripts . $script->innertext;

                            }

                        }

                    }

                    return response($scripts, 200)->header('Content-Type', 'text/javascript');

                }
            }

        }
    }

});

Route::get('b4bulder-custom-blocks.js', function () {

    $scripts = 'function PB4CustomBlocks(editor, options){' . PHP_EOL;

    $scripts = $scripts . 'var CustomBlock = editor.BlockManager;' . PHP_EOL;

    $scripts = $scripts . 'var Editor = editor;' . PHP_EOL;

    $scripts = $scripts . Settings::get('customblocks');

    $scripts = $scripts . '}';

    

    return response($scripts, 200)->header('Content-Type', 'text/javascript');

});
