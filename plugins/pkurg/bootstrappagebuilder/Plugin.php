<?php namespace Pkurg\BootstrapPageBuilder;

use Cms\Classes\Controller as CMSController;
use Cms\Classes\Page;
use Cms\Classes\Theme;
use Config;
use Event;
use Illuminate\Support\Facades\File;
use KubAT\PhpSimple\HtmlDomParser;
use Pkurg\BootstrapPageBuilder\Models\Settings;
use RainLab\Pages\Classes\Page as StaticPage;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;

class Plugin extends PluginBase
{

    public function registerMarkupTags()
    {
        return [
            'filters' => [

                'theme' => [$this, 'CanvasAssets'],
            ],

        ];
    }

    public function CanvasAssets($url)
    {

        $Controller = new CMSController;
        return $Controller->themeUrl($url);

    }

    public function registerComponents()
    {
        return [
            'Pkurg\BootstrapPageBuilder\Components\ContentBuilder' => 'ContentBuilder',
        ];
    }

    public function registerPermissions()
    {
        return [
            'pkurg.bootstrappagebuilder.manage' => [
                'tab' => 'Page Builder',
                'label' => 'Manage Page Builder',
            ],

        ];
    }

    public function registerSettings()
    {

        return [
            'settings' => [
                'label' => 'Bootstrap 4 Page Builder',
                'description' => 'Manage Page Builder settings.',
                'category' => SettingsManager::CATEGORY_CMS,
                'icon' => 'oc-icon-building-o',
                'class' => 'Pkurg\BootstrapPageBuilder\Models\Settings',
                'order' => 500,
                'permissions' => ['pkurg.bootstrappagebuilder.manage'],

            ],
        ];

    }

    public function registerFormWidgets()
    {
        return [
            'Pkurg\BootstrapPageBuilder\FormWidgets\Editor' => 'BootstrapPageBuilder',

        ];
    }

    public function addTabs()
    {

        // Extend all backend form usage
        Event::listen('backend.form.extendFields', function ($widget) {

            // Only for the CMS Index controller
            if (!$widget->getController() instanceof \Cms\Controllers\Index) {
                return;
            }

            // Only for the Page model
            // if (!($widget->model instanceof \Cms\Classes\Page or $widget->model instanceof \Cms\Classes\Partial)) {
            //     return;
            // }
            if (!$widget->model instanceof \Cms\Classes\Page) {
                return;
            }

            if (false === $widget->isNested) {

                // Add custom fields...
                $fields = [

                    'viewBag[page_builder_styles]' => [

                        'cssClass' => 'pkurg',
                        'type' => 'textarea',
                        'tab' => 'Styles',

                    ],
                    ['type' => 'partial',
                        'tab' => 'Styles',
                        'path' => '$/pkurg/bootstrappagebuilder/partials/slylesavebutton.htm',

                    ],

                    'viewBag[page_builder_html]' => [
                        'cssClass' => 'pkurg',
                        'type' => 'textarea',
                        'tab' => 'HTML',
                    ],
                    ['type' => 'partial',
                        'tab' => 'HTML',
                        'path' => '$/pkurg/bootstrappagebuilder/partials/htmlsavebutton.htm',

                    ],

                    'viewBag[page_builder_scripts]' => [

                        'cssClass' => 'pkurg',
                        'type' => 'textarea',
                        'tab' => 'Scripts',

                    ],
                    ['type' => 'partial',
                        'tab' => 'Scripts',
                        'path' => '$/pkurg/bootstrappagebuilder/partials/scriptsavebutton.htm',

                    ],

                ];

                //nosave layers names
                if (Settings::get('savelayersname')) {

                    $fields['viewBag[page_builder_components]'] = [

                        'cssClass' => 'pkurg',
                        'type' => 'textarea',
                        'tab' => 'B4Components',

                    ];

                }

                $widget->addSecondaryTabFields($fields);
            }

        });

    }

    public function addTabsWhenBuilderDisabled()
    {

        // Extend all backend form usage
        Event::listen('backend.form.extendFields', function ($widget) {

            // Only for the CMS Index controller
            if (!$widget->getController() instanceof \Cms\Controllers\Index) {
                return;
            }

            if (!$widget->model instanceof \Cms\Classes\Page) {
                return;
            }

            if (false === $widget->isNested) {

                // Add custom fields...
                $widget->addSecondaryTabFields([

                    'viewBag[page_builder_styles]' => [

                        'cssClass' => 'pkurg',
                        'type' => 'codeeditor',
                        'tab' => 'Styles',
                        'stretch' => 1,
                    ],

                    'viewBag[page_builder_scripts]' => [

                        'cssClass' => 'pkurg',
                        'type' => 'codeeditor',
                        'tab' => 'Scripts',
                        'stretch' => 1,

                    ],

                ]);

            }

        });

    }

    public function addTabsStaticPages()
    {
        // Static pages
        Event::listen('backend.form.extendFields', function ($widget) {

            // Only for the CMS Index controller
            if (!$widget->getController() instanceof \RainLab\Pages\Controllers\Index) {
                return;
            }

            // Only for the Page model
            if (!$widget->model instanceof \RainLab\Pages\Classes\Page) {
                return;
            }

            if (false === $widget->isNested) {

                // Add custom fields...

                $fields = [

                    'viewBag[page_builder_styles]' => [

                        'cssClass' => 'pkurg',
                        'type' => 'textarea',
                        'tab' => 'Styles',

                    ],
                    ['type' => 'partial',
                        'tab' => 'Styles',
                        'path' => '$/pkurg/bootstrappagebuilder/partials/slylesavebutton.htm',

                    ],

                    'viewBag[page_builder_html]' => [
                        'cssClass' => 'pkurg',
                        'type' => 'textarea',
                        'tab' => 'HTML',
                    ],
                    ['type' => 'partial',
                        'tab' => 'HTML',
                        'path' => '$/pkurg/bootstrappagebuilder/partials/htmlsavebutton.htm',

                    ],

                    'viewBag[page_builder_scripts]' => [

                        'cssClass' => 'pkurg',
                        'type' => 'textarea',
                        'tab' => 'Scripts',

                    ],
                    ['type' => 'partial',
                        'tab' => 'Scripts',
                        'path' => '$/pkurg/bootstrappagebuilder/partials/scriptsavebutton.htm',

                    ],

                ];

                if (Settings::get('savelayersname')) {

                    $fields['viewBag[page_builder_components]'] = [

                        'cssClass' => 'pkurg',
                        'type' => 'textarea',
                        'tab' => 'B4Components',

                    ];

                }

                $widget->addSecondaryTabFields($fields);

            }

        });

    }

    public function addTabsStaticPagesWhenDisabled()
    {
        // Static pages
        Event::listen('backend.form.extendFields', function ($widget) {

            // Only for the CMS Index controller
            if (!$widget->getController() instanceof \RainLab\Pages\Controllers\Index) {
                return;
            }

            // Only for the Page model
            if (!$widget->model instanceof \RainLab\Pages\Classes\Page) {
                return;
            }

            if (false === $widget->isNested) {

                // Add custom fields...
                $widget->addSecondaryTabFields([

                    'viewBag[page_builder_styles]' => [

                        'cssClass' => 'pkurg',
                        'type' => 'codeeditor',
                        'tab' => 'Styles',
                        'stretch' => 1,

                    ],

                    'viewBag[page_builder_scripts]' => [

                        'cssClass' => 'pkurg',
                        'type' => 'codeeditor',
                        'tab' => 'Scripts',
                        'stretch' => 1,

                    ],

                ]);

            }

        });

    }

    public function boot()
    {

        //default save images to Media library
        if (is_null(Settings::get('savelocal'))) {

            Settings::set('savelocal', 1);
        }

        //default save layers names
        if (is_null(Settings::get('savelayersname'))) {

            Settings::set('savelayersname', 1);
        }

        //delete builder/partialor content when del partial or content
        Event::listen('cms.template.delete', function (\Cms\Controllers\Index $controller, string $type) {

            if (($type == 'partial')) {

                $type = 'partials';

                $path = themes_path();

                $theme = \Cms\Classes\Theme::getActiveTheme()->getDirName();

                $file = $path . '/' . $theme . '/b4builder' . '/' . $type;

                $builderfiles = [];

                if (file_exists($file)) {

                    foreach (File::allFiles($file) as $f) {

                        $builderfiles[$f->getRealPath()] = $f->getRelativePathname();

                    }
                }

                $themefiles = [];

                $file = $path . '/' . $theme . '/' . $type;
                if (file_exists($file)) {

                    foreach (File::allFiles($file) as $f) {

                        $themefiles[$f->getRealPath()] = $f->getRelativePathname();

                    }
                }

                //$file = $path . '/' . $theme . '/' . $type;

                foreach ($builderfiles as $key => $value) {

                    $str = $value;
                    $exist = in_array($str, $themefiles);

                    if (!$exist) {

                        @unlink($key);

                    }

                }
            }
            if (($type == 'content')) {

                //delete builder/content when del partial
                $type = 'content';

                $path = themes_path();

                $theme = \Cms\Classes\Theme::getActiveTheme()->getDirName();

                $file = $path . '/' . $theme . '/b4builder' . '/' . $type;

                $builderfiles = [];

                if (file_exists($file)) {

                    foreach (File::allFiles($file) as $f) {

                        $builderfiles[$f->getRealPath()] = $f->getRelativePathname();

                    }
                }

                $themefiles = [];

                $file = $path . '/' . $theme . '/' . $type;

                if (file_exists($file)) {

                    foreach (File::allFiles($file) as $f) {

                        $themefiles[$f->getRealPath()] = $f->getRelativePathname();

                    }
                }

                //$file = $path . '/' . $theme . '/' . $type;

                foreach ($builderfiles as $key => $value) {

                    $str = $value;
                    $exist = in_array($str, $themefiles);

                    if (!$exist) {

                        @unlink($key);

                    }

                }

            }

        });

        Event::listen('cms.page.beforeRenderPartial', function (\Cms\Classes\Controller $controller, string $partialName) {

            //not add page style in edut mode
            if (\Input::has('b4builder-edit-mode')) {

                $isedit = \Input::get('b4builder-edit-mode');
                if ($isedit == '1') {
                    return;
                }
            }

            $pos = strripos($partialName, ':');
            //partial not component
            if (($pos === false)) {

                $controller->addCss(url('/') . '/b4bulder.css?type=partial&file=' . $partialName);

            }

        });

        Event::listen('cms.page.beforeRenderContent', function (\Cms\Classes\Controller $controller, string $contentName) {

            //not add page style in edut mode
            if (\Input::has('b4builder-edit-mode')) {

                $isedit = \Input::get('b4builder-edit-mode');
                if ($isedit == '1') {
                    return;
                }
            }

            $controller->addCss(url('/') . '/b4bulder.css?type=content&file=' . $contentName);

        });

        Event::listen('cms.page.beforeRenderPage', function (\Cms\Classes\Controller $controller, \Cms\Classes\Page $page) {

            if (Settings::get('show_static_page')) {

                if (isset($page->apiBag['staticPage']->viewBag['page_builder_styles'])) {

                    $controller->addCss(url('/') . '/b4bulder.css?static-page=' . $page->apiBag['staticPage']->fileName);

                }

                if (isset($page->apiBag['staticPage']->viewBag['page_builder_scripts'])) {

                    $template = StaticPage::load(Theme::getActiveTheme(), $page->apiBag['staticPage']->fileName);

                    if (array_key_exists('page_builder_scripts', $template->viewBag)) {

                        $s = $template->viewBag['page_builder_scripts'];

                        $html = HtmlDomParser::str_get_html($s);

                        if ($html) {
                            $scripts = '';
                            foreach ($html->find('script') as $script) {

                                if ($script->innertext == '') {

                                    $controller->addJs($script->src);

                                }

                            }

                        }
                    }

                    $controller->addJs(url('/') . '/b4bulder.js?static-page=' . $page->apiBag['staticPage']->fileName);

                }

            }

            //Add Styles assets
            if (array_key_exists('page_builder_styles', $page->viewBag)) {
                $controller->addCss(url('/') . '/b4bulder.css?page=' . $page->fileName);
            }

            //Add Scripts assets
            if (array_key_exists('page_builder_scripts', $page->viewBag)) {

                $template = Page::load(Theme::getActiveTheme(), $page->fileName);

                if (array_key_exists('page_builder_scripts', $template->viewBag)) {

                    $s = $template->viewBag['page_builder_scripts'];

                    $html = HtmlDomParser::str_get_html($s);

                    if ($html) {
                        $scripts = '';
                        foreach ($html->find('script') as $script) {

                            if ($script->innertext == '') {

                                $controller->addJs($script->src);

                            }

                        }

                    }

                }

                $controller->addJs(url('/') . '/b4bulder.js?page=' . $page->fileName);

            }

        });

        if (Settings::get('show_page')) {
            $this->addTabs();
        } else {

            $this->addTabsWhenBuilderDisabled();
        }

        if (Settings::get('show_static_page')) {
            $this->addTabsStaticPages();
        } else {
            $this->addTabsStaticPagesWhenDisabled();
        }

        Event::listen('backend.form.extendFields', function ($form) {

            if (get_class($form->config->model) == 'Cms\Classes\Page' and Settings::get('show_page')) {

                replaceEditor($form);

            }

            if (get_class($form->config->model) == 'Cms\Classes\Content' and Settings::get('show_content')) {

                replaceEditor($form);
            }
            if (get_class($form->config->model) == 'Cms\Classes\Partial' and Settings::get('show_partial')) {

                replaceEditor($form);
            }
            if (get_class($form->config->model) == 'Cms\Classes\Layout' and Settings::get('show_layout')) {

                replaceEditor($form);
            }

            //Static pages
            if (Settings::get('show_static_page', false) && $form->model instanceof \RainLab\Pages\Classes\Page) {

                replaceEditor($form);
            }

        });

        function replaceEditor($form)
        {
            $replacable = [
                'codeeditor', 'Eein\Wysiwyg\FormWidgets\Trumbowyg', 'richeditor', 'RainLab\Blog\FormWidgets\BlogMarkdown',
                'RainLab\Blog\FormWidgets\MLBlogMarkdown', 'mlricheditor',
            ];

            $multilanguage = [
                'RainLab\Blog\FormWidgets\MLBlogMarkdown', 'mlricheditor',
            ];

            foreach ($form->getFields() as $field) {

                if (!empty($field->config['type']) && $field->config['type'] == 'codeeditor' && $field->fieldName == 'markup') {

                    $field->config['type'] = $field->config['widget'] = 'BootstrapPageBuilder';

                }

                //static pages
                if (!empty($field->config['type']) and (($field->config['type'] == 'mlricheditor') or ($field->config['type'] == 'richeditor')) and $field->tab == 'rainlab.pages::lang.editor.content') {

                    $field->config['type'] = $field->config['widget'] = 'BootstrapPageBuilder';

                }

            }
        }

    }

}
