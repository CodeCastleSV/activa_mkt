<?php return [
    'plugin' => [
        'name' => 'Bootstrap 4 Page Builder',
        'description' => 'Building Bootstrap 4 pages without coding'
    ]
];