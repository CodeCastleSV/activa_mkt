<?php namespace Pkurg\BootstrapPageBuilder\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Cms\Classes\Content;
use Cms\Classes\Partial;
use Config;
use Pkurg\BootstrapPageBuilder\Models\Settings;
use Storage;

class Editor extends FormWidgetBase
{

    /**
     * @var string A unique alias to identify this widget.
     */
    protected $defaultAlias = 'BootstrapPageBuilder';

    // public function getScripts()
    // {
    //     return 'scripts';
    // }

    public function render()
    {

        $curdir = url('/');

        $this->addCss($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/css/grapes.min.css?v=1');

        $this->addCss($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/css/grapick.min.css?v=8');

        $this->addCss($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/builder.css?v=12');

        $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/grapes.min.js?v=5');

        $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/vkbeautify.js');

        $this->addJs('https://b4builder.pkurg.ru/data/grapesjs-pkurg-bootstrap4-plugin.js?v=3');
        $this->addJs('https://b4builder.pkurg.ru/data/blog.js?v=89');

        $this->addJs('https://b4builder.pkurg.ru/data/grapesjs-pkurg-bootstrap4-templates-plugin.js?v=2');

        $this->addJs('https://b4builder.pkurg.ru/data/grapesjs-pkurg-bootstrap4-plugin-snippets.js');

        $this->addJs('https://b4builder.pkurg.ru/data/grapesjs-pkurg-bootstrap4-plugin-landing-pages.js');

        $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/grapes-custom-code.js');

        $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/grapesjs-tui-image-editor.min.js');

        $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/grapesjs-style-gradient.min.js');

        $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/grapesjs-style-filter.min.js');

        $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/october.js?v=1');

        $u = uniqid();

        $this->addJs(url('b4bulder-custom-blocks.js?v=' . $u));

        $defaultdisk = Config::get('filesystems.default');

        $path = url(Config::get('filesystems.disks.' . $defaultdisk . '.url'));

        $files = Storage::allFiles('media/BuilderUploader');

        $purl = parse_url($path);

        $this->vars['path'] = $purl['path'];

        $this->vars['files'] = $files;

        $this->vars['formid'] = $this->formField->idPrefix;

        $this->vars['name'] = $this->getFieldName();
        $this->vars['value'] = $this->getLoadValue();

        $this->vars['curdir'] = url('/');

        $this->vars['pagename'] = $this->model->fileName;

        //$this->vars['customblocks'] = Settings::get('customblocks');

        $this->vars['mediapath'] = config('cms.storage.media.path');

        $this->vars['partials'] = $this->getPartials();

        $this->vars['contents'] = $this->getContents();

        $this->vars['octobermedia'] = Settings::get('useoctobermedia');

        $defaultdisk = Config::get('filesystems.default');

        $path = url(Config::get('filesystems.disks.' . $defaultdisk . '.url'));

        $purl = parse_url($path);

        $mediadir = Config::get('cms.storage.media.folder');

        //$Url = $purl['path'] . '/' . $mediadir . '/BuilderUploader/' . $q . $fileName;
        $this->vars['mediadir'] = $purl['path'] . '/' . $mediadir;

        $this->vars['buildercanvasassets'] = Settings::get('buildercanvasassets');

        if ($this->vars['buildercanvasassets']) {

            if (Settings::get('builder_styles')) {
                $this->vars['builder_styles'] = \Twig::parse(Settings::get('builder_styles'));
            } else {
                $this->vars['builder_styles'] = '';
            }

            if (Settings::get('builder_scripts')) {
                $this->vars['builder_scripts'] = \Twig::parse(Settings::get('builder_scripts'));
            } else {
                $this->vars['builder_scripts'] = '';
            }
        }

        return $this->makePartial('builder');
    }

    public function getPartials()
    {

        $a = Partial::sortBy('baseFileName')->lists('baseFileName', 'fileName');

        $b = [];

        foreach ($a as $key => &$value) {

            $l = strlen($value);
            if (mb_substr($value, $l - 3, 1) == '.') {

                $s = mb_substr($value, 0, $l - 3);

            } else {

                $b[$key] = $value;

            }

        }

        return $b;

    }

    public function getContents()
    {

        $a = Content::sortBy('baseFileName')->lists('baseFileName', 'fileName');

        $b = [];

        foreach ($a as $key => &$value) {

            $l = strlen($value);
            if (mb_substr($value, $l - 3, 1) == '.') {

                $s = mb_substr($value, 0, $l - 3);
                //$b[$s] = $s;

            } else {

                $b[$key] = $value;

            }

        }

        return $b;
    }

}
