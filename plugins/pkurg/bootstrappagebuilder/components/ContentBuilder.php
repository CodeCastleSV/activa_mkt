<?php namespace Pkurg\BootstrapPageBuilder\Components;

use BackendAuth;
use Cms\Classes\ComponentBase;
use Cms\Classes\Content;
use Cms\Classes\Partial;
use Config;
use File;
use Input;
use KubAT\PhpSimple\HtmlDomParser;
use Lang;
use Pkurg\BootstrapPageBuilder\Models\Settings;
use RainLab\Translate\Classes\Translator;
use Request;
use Response;
use Storage;
use System\Classes\PluginManager;

class ContentBuilder extends ComponentBase
{

    public $content;
    public $file;
    public $fileMode;
    public $edit;
    public $id;
    public $jsoncontent;
    public $json;

    public $isbuilderedit;

    /////////////////////////////Builder

    public function componentDetails()
    {
        return [
            'name' => 'ContentBuilder Component',
            'description' => 'Frontend content builder',
        ];
    }

    public function defineProperties()
    {
        return [
            'savedatato' => [
                'title' => 'Save page data to',
                'type' => 'dropdown',
                'default' => 'content',
            ],

            'file' => [
                'title' => 'File',
                'type' => 'dropdown',
                'depends' => ['savedatato'],

            ],
            'button_text' => [
                'title' => 'Button text',
                'description' => '',
                'type' => 'string',
            ],
            'canvas_styles' => [
                'title' => 'Canvas Styles',
                'description' => '',
                'type' => 'stringList',
                'showExternalParam' => false,
            ],
            'canvas_scripts' => [
                'title' => 'Canvas Scripts',
                'description' => '',
                'type' => 'stringList',
                'showExternalParam' => false,
            ],

            'readOnly' => [
                'title' => 'Read only',
                'type' => 'checkbox',
                'default' => false,
            ],

            'rootdir' => [
                'title' => 'Media root dir',

                'default' => 'media',

            ],

        ];
    }

    public function getSavedatatoOptions()
    {
        return ['content' => 'content', 'partial' => 'partial'];
    }
    public function getFileOptions()
    {

        $Code = Request::input('savedatato');

        if ($Code == 'content') {

            if (PluginManager::instance()->exists('RainLab.Translate')) {

                $a = Content::sortBy('baseFileName')->lists('baseFileName', 'fileName');

                $b = [];

                foreach ($a as $key => &$value) {

                    $l = strlen($value);
                    if (mb_substr($value, $l - 3, 1) == '.') {

                        $s = mb_substr($value, 0, $l - 3);
                        $b[$s] = $s;

                    } else {

                        $b[$key] = $value;
                    }

                }

                return array_unique($b);
            } else {

                return Content::sortBy('baseFileName')->lists('baseFileName', 'fileName');
            }
        }

        if ($Code == 'partial') {

            if (PluginManager::instance()->exists('RainLab.Translate')) {

                $a = Partial::sortBy('baseFileName')->lists('baseFileName', 'fileName');

                $b = [];

                foreach ($a as $key => &$value) {

                    $l = strlen($value);
                    if (mb_substr($value, $l - 3, 1) == '.') {

                        $s = mb_substr($value, 0, $l - 3);
                        $b[$s] = $s;

                    } else {

                        $b[$key] = $value;
                    }

                }

                return array_unique($b);

            } else {

                return Partial::sortBy('baseFileName')->lists('baseFileName', 'fileName');

            }

        }

    }

    public function startsWith($string, $startString)
    {
        $len = strlen($startString);
        return (substr($string, 0, $len) === $startString);
    }

    public function getStyles($html)
    {

        //get styles
        $html = HtmlDomParser::str_get_html($html);
        if ($html) {
            $styles = '';
            foreach ($html->find('style') as $style) {
                $styles = $styles . $style->innertext;
            }

            return '<style type="text/css">' . $styles . '</style>';
        }

    }

    public function getHTML($html)
    {

        $html = HtmlDomParser::str_get_html($html);

        //remove slyles and scripts
        if ($html) {

            foreach ($html->find('script') as $script) {
                $script->remove();
            }

            foreach ($html->find('style') as $script) {
                $script->remove();
            }

            return $html->innertext;
        }

    }

    public function getScripts($html)
    {

        $html = HtmlDomParser::str_get_html($html);

        if ($html) {
            $scripts = '';
            foreach ($html->find('script') as $script) {
                $scripts = $scripts . $script;
            }
            return $scripts;
        }

    }

    public function onRun()
    {

        $this->id = uniqid();

        $this->edit = $this->isEdit();

        $this->page['editmode'] = Input::get('b4builder-edit-mode');

        if (!$this->isbuilderedit) {

            if ($this->edit) {

                $curdir = url('/');

                $this->addCss($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/builder.css?v=33');
            }

            $this->file = $this->property('file');

            $this->fileMode = File::extension($this->property('file'));

            if (PluginManager::instance()->exists('RainLab.Translate')) {

                $translator = Translator::instance();
                $activeLocale = $translator->getLocale();

                if (!$this->fileMode) {

                    $this->file = $this->file . '.' . $activeLocale . '.htm';

                }

            }

            if ($this->property('file') == null) {

                $this->content = 'No file';

            } else {

                if ($this->property('savedatato') == 'content') {

                    $this->content = $this->renderContent($this->file);
                    //$this->jsoncontent = json_encode($this->content);

                }

                if ($this->property('savedatato') == 'partial') {

                    $this->content = $this->renderPartial($this->file);
                    //$this->jsoncontent = json_encode($this->content);

                }

            }

            return;
        }

        $this->page['editor'] = Input::get('editor');

        $defaultdisk = Config::get('filesystems.default');

        $path = url(Config::get('filesystems.disks.' . $defaultdisk . '.url'));

        $files = Storage::allFiles('media/BuilderUploader');

        $purl = parse_url($path);

        $this->page['path'] = $purl['path'];

        $this->page['files'] = $files;

        $this->page['octobermedia'] = Settings::get('useoctobermedia');

        $defaultdisk = Config::get('filesystems.default');

        $path = url(Config::get('filesystems.disks.' . $defaultdisk . '.url'));

        $purl = parse_url($path);

        $mediadir = Config::get('cms.storage.media.folder');

        $this->page['mediadir'] = $purl['path'] . '/' . $mediadir;

        $this->page['curdir'] = url('/');

        $this->page['mediapath'] = config('cms.storage.media.path');

        $this->page['backendpath'] = config('cms.backendUri');

        $curdir = url('/');

        $this->file = $this->property('file');

        $this->fileMode = File::extension($this->property('file'));

        if (PluginManager::instance()->exists('RainLab.Translate')) {

            $translator = Translator::instance();
            $activeLocale = $translator->getLocale();

            if (!$this->fileMode) {

                $this->file = $this->file . '.' . $activeLocale . '.htm';

            }

        }

        if ($this->property('file') == null) {

            $this->content = 'No file';

        } else {

            if ($this->property('savedatato') == 'content') {

                $template = Content::load($this->getTheme(), $this->file);

                $this->content = $template->markup;

                $this->jsoncontent = json_encode($this->content);

                $fileNameJSON = $this->file;

                $type = 'content/';

                $path = themes_path();

                $theme = \Cms\Classes\Theme::getActiveTheme()->getDirName();

                $file = $path . '/' . $theme . '/b4builder' . '/' . $type . $fileNameJSON;

                if (Settings::get('savelayersname')) {

                    $template = @file_get_contents($file);

                } else {

                    $template = false;
                }

                if (!$template) {

                    $this->json = '';

                } else {

                    $this->json = $template;
                }

            }

            if ($this->property('savedatato') == 'partial') {

                $template = Partial::load($this->getTheme(), $this->file);

                $this->content = $template->markup;

                $this->jsoncontent = json_encode($this->content);

                $fileNameJSON = $this->file;

                $type = 'partials/';

                $path = themes_path();

                $theme = \Cms\Classes\Theme::getActiveTheme()->getDirName();

                $file = $path . '/' . $theme . '/b4builder' . '/' . $type . $fileNameJSON;

                if (Settings::get('savelayersname')) {

                    $template = @file_get_contents($file);

                } else {

                    $template = false;
                }

                if (!$template) {

                    $this->json = '';

                } else {

                    $this->json = $template;

                }

            }

        }

        if ($this->isEdit()) {

            foreach ($this->assets['css'] as $value) {
                $this->page->addCss($value, 'core');
            }

            foreach ($this->assets['js'] as $value) {
                $this->page->addJs($value, 'core');
            }

            $this->addCss($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/css/grapes.min.css?v=7');

            $this->addCss($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/css/grapick.min.css?v=8');

            $this->addCss($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/builder.css?v=33');

            $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/grapes.min.js?v=1');

            $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/vkbeautify.js');

            $this->addJs('https://b4builder.pkurg.ru/data/grapesjs-pkurg-bootstrap4-plugin.js?v=2');

            $this->addJs('https://b4builder.pkurg.ru/data/blog.js?v=89');

            $this->addJs('https://b4builder.pkurg.ru/data/grapesjs-pkurg-bootstrap4-templates-plugin.js?v=22');

            $this->addJs('https://b4builder.pkurg.ru/data/grapesjs-pkurg-bootstrap4-plugin-snippets.js');

            $this->addJs('https://b4builder.pkurg.ru/data/grapesjs-pkurg-bootstrap4-plugin-landing-pages.js');

            $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/grapes-custom-code.js');

            $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/grapesjs-tui-image-editor.min.js');

            $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/b4utils.js');

            $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/grapesjs-style-gradient.min.js');

            $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/grapesjs-style-filter.min.js');

            $this->addJs($curdir . '/plugins/pkurg/bootstrappagebuilder/formwidgets/editor/assets/october.js?v=1');

            $this->addJs(url('b4bulder-custom-blocks.js'));

        }

    }

    public function onSaveFile()
    {
        if (!$this->isEdit()) {
            return;
        }

        $fileName = post('file');

        if ($this->property('savedatato') == 'partial') {

            if (Settings::get('savelayersname')) {

                $fileNameJSON = $fileName;

                $type = 'partials/';

                $path = themes_path();

                $theme = \Cms\Classes\Theme::getActiveTheme()->getDirName();

                $file = $path . '/' . $theme . '/b4builder' . '/' . $type . $fileNameJSON;

                $content = post('json');

                $content = str_replace('{#', '{ #', $content);
                $content = str_replace('/script', '\/script', $content);

                if (!file_exists(dirname($file))) {
                    mkdir(dirname($file), 0777, true);
                }

                $r = @file_put_contents($file, $content);

                if (!$r) {

                    return Response::make('components file not saved', 403);
                }

            }

            //Save content
            $template = Partial::load($this->getTheme(), $fileName);
            $content = post('content');
            $content = str_replace('{#', '{ #', $content);
            $template->fill(['markup' => $content]);
            $template->save();
        }

        if ($this->property('savedatato') == 'content') {

            if (Settings::get('savelayersname')) {

                $fileNameJSON = $fileName;

                $type = 'content/';

                $path = themes_path();

                $theme = \Cms\Classes\Theme::getActiveTheme()->getDirName();

                $file = $path . '/' . $theme . '/b4builder' . '/' . $type . $fileNameJSON;

                $content = post('json');

                $content = str_replace('{#', '{ #', $content);
                $content = str_replace('/script', '\/script', $content);

                if (!file_exists(dirname($file))) {
                    mkdir(dirname($file), 0777, true);
                }

                $r = @file_put_contents($file, $content);

                if (!$r) {

                    return Response::make('components file not saved', 403);
                }
            }

            //Save content
            $template = Content::load($this->getTheme(), $fileName);
            $content = post('content');
            $content = str_replace('{#', '{ #', $content);
            $template->fill(['markup' => $content]);
            $template->save();
        }
    }

    public function isEdit()
    {
        $backendUser = BackendAuth::getUser();
        return $backendUser && ($backendUser->hasAccess('cms.manage_content') || $backendUser->hasAccess('rainlab.pages.manage_content'));
    }

    public function getPartials()
    {

        $a = Partial::sortBy('baseFileName')->lists('baseFileName', 'fileName');

        $b = [];

        foreach ($a as $key => &$value) {

            $l = strlen($value);
            if (mb_substr($value, $l - 3, 1) == '.') {

                $s = mb_substr($value, 0, $l - 3);

            } else {

                if ($key != $this->property('file')) {
                    $b[$key] = $value;
                }

            }

        }

        return $b;

    }

    public function getContents()
    {

        $a = Content::sortBy('baseFileName')->lists('baseFileName', 'fileName');

        $b = [];

        foreach ($a as $key => &$value) {

            $l = strlen($value);
            if (mb_substr($value, $l - 3, 1) == '.') {

                $s = mb_substr($value, 0, $l - 3);
                //$b[$s] = $s;

            } else {

                if ($key != $this->property('file')) {
                    $b[$key] = $value;
                }
            }

        }

        return $b;
    }

////////////////MEdia

    public $mediaManagerWidget;

    public $mediaFinder;

    public $assets = [];

    public $c;

    public function init()
    {

        if (\Input::has('b4builder-edit-mode')) {

            $isedit = \Input::get('b4builder-edit-mode');
            if ($isedit == '1') {

                $this->isbuilderedit = true;
            }
        }

        if ($this->isbuilderedit) {

            $this->assets['css'] = [
                '/modules/system/assets/ui/storm.css',
                '/modules/backend/assets/css/october.css',
            ];

            $this->assets['js'] = [
                '/modules/system/assets/js/lang/lang.' . Lang::getLocale() . '.js',
                '/modules/system/assets/ui/storm-min.js',
                '/modules/backend/assets/js/october-min.js',
            ];

            $this->c = new \Backend\Classes\Controller();
            $this->mediaManagerWidget = new \Backend\Widgets\MediaManager($this->c, $this->alias, $this->property('readOnly'));
            $this->bindHandlers($this->mediaManagerWidget);

            $this->mergeAssets();
        } else {

            $this->assets['css'] = [];

            $this->assets['js'] = [];

            $this->mergeAssets();

        }
    }

    private function removeVersionsAssets($arrData)
    {
        if (is_array($arrData)) {
            foreach ($arrData as $key => $value) {
                if (is_array($value)) {
                    $arrData[$key] = $this->removeVersionsAssets($value);
                } else {
                    $arr = explode("?v", $value);
                    $arrData[$key] = (is_array($arr)) ? $arr[0] : $value;
                }
            }

        }

        return $arrData;
    }

    private function getAssets($widget)
    {
        $assets = $this->removeVersionsAssets($widget->getAssetPaths());
        $this->assets['css'] = (array_key_exists('css', $this->assets)) ? array_merge($this->assets['css'], $assets['css']) : $this->assets['css'] = $assets['css'];
        $this->assets['js'] = (array_key_exists('js', $this->assets)) ? array_merge($this->assets['js'], $assets['js']) : $this->assets['js'] = $assets['js'];
    }

    private function mergeAssets()
    {
        $this->page['assets'] = (is_array($this->page['assets'])) ? array_merge($this->assets, $this->page['assets']) : $this->page['assets'] = $this->assets;
    }

    private function bindHandlers($widget)
    {
        if (is_object($widget)) {
            $widget->viewPath = $this->property('viewPath');
            $this->bindHandlersNoPath($widget);
        }
    }

    private function bindHandlersNoPath($widget)
    {
        if (is_object($widget)) {
            $widget->alias = $this->alias;
            //$widget->viewPath = $this->property('viewPath');
            $class_methods = get_class_methods($widget);

            foreach ($class_methods as $method_name) {
                if (strtolower(substr($method_name, 0, 2)) == 'on') {
                    //array_push($this->relationsarray,$method_name);
                    $this->addDynamicMethod($method_name, function () use ($method_name, $widget) {
                        return $widget->{$method_name}();
                    });
                }
            }

            $this->getAssets($widget);
        }
    }

    public function getSetting($setting)
    {

        if (Settings::get($setting)) {
            return \Twig::parse(Settings::get($setting));
        } else {
            return '';

        }

    }

}
