<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\laragon\www\activa_mkt/plugins/rainlab/blog/components/posts/default.htm */
class __TwigTemplate_ab0362b3f8c00c291a0c129630021e637169bc535acf8d2c157e7baca5989eb1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("set" => 1, "for" => 11, "if" => 15);
        $filters = array("escape" => 20, "raw" => 32, "page" => 53);
        $functions = array("range" => 57);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'for', 'if'],
                ['escape', 'raw', 'page'],
                ['range']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["posts"] = twig_get_attribute($this->env, $this->source, ($context["__SELF__"] ?? null), "posts", [], "any", false, false, true, 1);
        // line 2
        echo "
<ul class=\"post-list\"  style=\"padding:10px;\">
<div class=\"container \">
        <div class=\"row mb-5\">
            <div data-aos=\"fade\" class=\"col-12 text-center \">
                <h2 style=\"color: #0b666e; \" class=\"section-title mb-3 ff ft\">Blog</h2>
            </div>
        </div>
        <div class=\"row\">
    ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 12
            echo "        
            <div   style=\"padding-left:20px;\" data-aos=\"fade-up\" data-aos-delay=\"\" id=\"myDIV\" class=\"  col-12 col-md-6 col-lg-4 mb-4 mb-lg-4\">
                <div class=\"h-entry\">
                    ";
            // line 15
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "featured_images", [], "any", false, false, true, 15), "count", [], "any", false, false, true, 15)) {
                // line 16
                echo "    <div class=\"featured-images text-center \">
        ";
                // line 17
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["post"], "featured_images", [], "any", false, false, true, 17));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 18
                    echo "            <p>
                <img class=\"\"
                    data-src=\"";
                    // line 20
                    echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["image"], "filename", [], "any", false, false, true, 20), 20, $this->source), "html", null, true);
                    echo "\"
                    src=\"";
                    // line 21
                    echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["image"], "path", [], "any", false, false, true, 21), 21, $this->source), "html", null, true);
                    echo "\"
                    alt=\"";
                    // line 22
                    echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["image"], "description", [], "any", false, false, true, 22), 22, $this->source), "html", null, true);
                    echo "\"
                    style=\"max-width: 100%;max-height: 300px\" />
            </p>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 26
                echo "    </div>
";
            }
            // line 28
            echo "                    <h2 class=\"font-size-regular example\" ><a style=\"color: #0b666e;   z-index: 1\"  href=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["post"], "url", [], "any", false, false, true, 28), 28, $this->source), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["post"], "title", [], "any", false, false, true, 28), 28, $this->source), "html", null, true);
            echo "</a></h2>
                


                      <p class=\"excerp  ff\">";
            // line 32
            echo $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["post"], "summary", [], "any", false, false, true, 32), 32, $this->source);
            echo "</p>
                      
                     <a  class=\" ff font-weight-bold btn smoothscroll btn-danger mr-2 mb-2\" href=\"";
            // line 34
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["post"], "url", [], "any", false, false, true, 34), 34, $this->source), "html", null, true);
            echo "\" > <div data-aos=\"fade-up\" data-aos-delay=\"100\">Ver Más</div></a>
                </div>
            </div>
           
     

       
    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 42
            echo "        <li class=\"no-data\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["__SELF__"] ?? null), "noPostsMessage", [], "any", false, false, true, 42), 42, $this->source), "html", null, true);
            echo " </li>
        
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "     
       </div>
    </div>
</ul>

";
        // line 50
        if ((twig_get_attribute($this->env, $this->source, ($context["posts"] ?? null), "lastPage", [], "any", false, false, true, 50) > 1)) {
            // line 51
            echo "    <ul class=\"col-12 pagination d-flex justify-content-end \"  style=\"margin:0px; padding-right:10px\">
        ";
            // line 52
            if ((twig_get_attribute($this->env, $this->source, ($context["posts"] ?? null), "currentPage", [], "any", false, false, true, 52) > 1)) {
                // line 53
                echo "            <li  ><a style=\"color:#0b666e !important; background-color: white;\" class=\"btn btn-danger\" href=\"  ";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, true, 53), "baseFileName", [], "any", false, false, true, 53), 53, $this->source), [twig_get_attribute($this->env, $this->source, ($context["__SELF__"] ?? null), "pageParam", [], "any", false, false, true, 53) => (twig_get_attribute($this->env, $this->source, ($context["posts"] ?? null), "currentPage", [], "any", false, false, true, 53) - 1)]);
                echo "#blog-section \">&larr; Prev</a></li>
             <a href=\"\"></a>
        ";
            }
            // line 56
            echo "
        ";
            // line 57
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, twig_get_attribute($this->env, $this->source, ($context["posts"] ?? null), "lastPage", [], "any", false, false, true, 57)));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 58
                echo "            <li class=\"";
                echo (((twig_get_attribute($this->env, $this->source, ($context["posts"] ?? null), "currentPage", [], "any", false, false, true, 58) == $context["page"])) ? ("active") : (null));
                echo "\">
                <a style=\"color:#0b666e !important; background-color: white;\" class=\"\" href=\" ";
                // line 59
                echo $this->extensions['Cms\Twig\Extension']->pageFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, true, 59), "baseFileName", [], "any", false, false, true, 59), 59, $this->source), [twig_get_attribute($this->env, $this->source, ($context["__SELF__"] ?? null), "pageParam", [], "any", false, false, true, 59) => $context["page"]]);
                echo "#blog-section\">";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed($context["page"], 59, $this->source), "html", null, true);
                echo "</a>
            </li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 62
            echo "
        ";
            // line 63
            if ((twig_get_attribute($this->env, $this->source, ($context["posts"] ?? null), "lastPage", [], "any", false, false, true, 63) > twig_get_attribute($this->env, $this->source, ($context["posts"] ?? null), "currentPage", [], "any", false, false, true, 63))) {
                // line 64
                echo "            <li><a style=\"color:#0b666e !important; background-color: white;\" class=\"\" href=\"";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, true, 64), "baseFileName", [], "any", false, false, true, 64), 64, $this->source), [twig_get_attribute($this->env, $this->source, ($context["__SELF__"] ?? null), "pageParam", [], "any", false, false, true, 64) => (twig_get_attribute($this->env, $this->source, ($context["posts"] ?? null), "currentPage", [], "any", false, false, true, 64) + 1)]);
                echo "#blog-section\">Next &rarr;</a></li>
        ";
            }
            // line 66
            echo "    </ul>
";
        }
        // line 68
        echo "





";
    }

    public function getTemplateName()
    {
        return "C:\\laragon\\www\\activa_mkt/plugins/rainlab/blog/components/posts/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 68,  213 => 66,  207 => 64,  205 => 63,  202 => 62,  191 => 59,  186 => 58,  182 => 57,  179 => 56,  172 => 53,  170 => 52,  167 => 51,  165 => 50,  158 => 45,  148 => 42,  135 => 34,  130 => 32,  120 => 28,  116 => 26,  106 => 22,  102 => 21,  98 => 20,  94 => 18,  90 => 17,  87 => 16,  85 => 15,  80 => 12,  75 => 11,  64 => 2,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "C:\\laragon\\www\\activa_mkt/plugins/rainlab/blog/components/posts/default.htm", "");
    }
}
