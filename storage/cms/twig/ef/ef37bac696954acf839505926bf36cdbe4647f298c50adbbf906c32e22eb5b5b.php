<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\laragon\www\activa_mkt/plugins/activamkt/contact/components/contactform/default.htm */
class __TwigTemplate_16e8e0ea836c05a7f24129689f7093cdcec9350bd480b5bede43cb188d85e50f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("for" => 74);
        $filters = array("escape" => 75);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['for'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo " <section id=\"contact-section\" data-aos=\"fade\" class=\"site-section bg-light\">
       <style>
       .h2{
           font-size: 70px !important;
           }
       </style>
       
        <div class=\"container-fluid \">
            <div class=\"row mb-5\">
                <div class=\"col-12 text-center\">
                   
                </div>
            </div>
            <div class=\"row mb-5 \" style=\"margin-top:150px; background-color:#0b666e ; padding: 80px;\">
                <div class=\"col-md-4 text-center\">
                    <p class=\"mb-4\"><span class=\"icon-room d-block h2  text-white\"></span><span class=\"text-white\">203 Fake St. Mountain View, San Francisco, California, USA</span></p>
                </div>
                <div class=\"col-md-4 text-center\">
                  <p class=\"mb-4  text-white\"><span  class=\"icon-phone d-block h2  text-white\" ></span>  <a style='text-decoration:none;color:white;' href=\"https://wa.me/68282413\" target=\"_blank\"> +507 68282413</a></p>
                </div>
                <div class=\"col-md-4 text-center\">
                    <p class=\"mb-0\"><span class=\"icon-mail_outline d-block h2  text-white\"></span><a  style='text-decoration:none;color:white;'  href=\"mailto:contactanos@activa-mkt.com
\" target=\"_blank\">contactanos@activa-mkt.com
</a></p>
                </div>
            </div >

              <section id=\"form-section\"></section>
              <div class=\"container\">
            <div   class=\"row\" style=\"margin-top:200px ;\"  >
                <div  class=\"col-md-12 mb-5 \">
                <h1 class=\" mb-5 text-center section-title \">Completa nuestro formulario</h1>
                <div class=\"d-flex justify-content-center\">
                    <form data-request=\"onSend\" class=\"p-5 bg-white col-12 col-md-5 \">
                        
                        <div class=\"row form-group\">
                            <div class=\"col-md-12 mb-3 mb-md-0\"><label for=\"name\" class=\"text-black\">Nombre completo<div></div></label><input required type=\"text\" id=\"name\" name=\"name\" placeholder=\"Nombre completo\" class=\"form-control\" /></div>

                        </div>
                        <div class=\"row form-group\">
                            <div class=\"col-md-12\"><label for=\"email\" class=\"text-black\">Email<div></div></label><input required type=\"email\" id=\"email\" name=\"email\" placeholder=\"Email\" class=\"form-control\" /></div>
                        </div>
                        <div class=\"row form-group\">
                            <div class=\"col-md-12\"><label for=\"company\" class=\"text-black\">Empresa (opcional)<div></div></label><input type=\"text\" id=\"company\" name=\"company\" placeholder=\"Empresa\" class=\"form-control\" /></div>
                        </div>
                        <div class=\"row form-group\">
                            <div class=\"col-md-12\"><label for=\"phone\" class=\"text-black\">Teléfono<div></div></label><input type=\"text\" id=\"phone\" cols=\"30\" name=\"phone\" rows=\"7\" placeholder=\"tel:\" class=\"form-control\"></input>
                            </div>
                        </div>

                        <div class=\"row form-group\">
                            <div class=\"col-md-12\"><label for=\"service\" class=\"text-black\">servicio<div></div></label>
                            
                               <select  class=\"form-control\"  name=\"select\">
                               <option value=\"Desarrollos digitales\"> Desarrollos digitales</option> 
                               <option value=\"Diseño y creacion de contenido digital\">Diseño y creacion de contenifdo digital</option>
                               <option value=\"BRANDING\">BRANDING</option>
                               <option value=\"Analisis de entorno\">Analisis de entorno</option>
                                <option value=\"B-ACCESS\">B-ACCESS</option>

                              </select>

                        

                            </div>
                        </div>

                        <div class=\"row form-group\">
                            <div class=\"col-md-12\"><button type=\"submit\"  placeholder=\"Insert text here\" value=\"Enviar\" class=\"btn btn-danger btn-lg btn-block text-white\">Enviar</button></div>
                        </div>


                        <ul>
                        ";
        // line 74
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["errors"] ?? null), "all", [], "method", false, false, true, 74));
        foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
            // line 75
            echo "                        <li>";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed($context["error"], 75, $this->source), "html", null, true);
            echo "</li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "                        
                        </ul>
                    </form>
                    </div>
                </div>
            </div>
            </div>
        </div>


    </section>";
    }

    public function getTemplateName()
    {
        return "C:\\laragon\\www\\activa_mkt/plugins/activamkt/contact/components/contactform/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 77,  141 => 75,  137 => 74,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "C:\\laragon\\www\\activa_mkt/plugins/activamkt/contact/components/contactform/default.htm", "");
    }
}
