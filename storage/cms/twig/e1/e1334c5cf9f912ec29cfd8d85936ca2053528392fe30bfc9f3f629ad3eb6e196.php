<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\laragon\www\activa_mkt/plugins/sub/form/components/contactform2/default.htm */
class __TwigTemplate_0dc2bf283119e64be7560deda1586633ed7a9321170278f527c2a7076d9e5190 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("for" => 11);
        $filters = array("escape" => 12);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['for'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "   <form data-request=\"onSend2\"  class=\"footer-subscribe\">
                        <div class=\"input-group mb-3\"><input required type=\"email\" id=\"email\" name=\"email\" placeholder=\"Email\" class=\"form-control  border-secondary text-white bg-transparent\" />
                              <div class=\"row form-group\">
                            <div class=\"col-md-6\"><button type=\"submit\"  placeholder=\"Insert text here\" value=\"Enviar\" class=\"btn btn-primary text-black\">Enviar</button></div>
                        </div>


                     
                        </div>
                           <ul>
                        ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["errors"] ?? null), "all", [], "method", false, false, true, 11));
        foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
            // line 12
            echo "                        <li>";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed($context["error"], 12, $this->source), "html", null, true);
            echo "</li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "                        
                        </ul>
                    </form>";
    }

    public function getTemplateName()
    {
        return "C:\\laragon\\www\\activa_mkt/plugins/sub/form/components/contactform2/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 14,  78 => 12,  74 => 11,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "C:\\laragon\\www\\activa_mkt/plugins/sub/form/components/contactform2/default.htm", "");
    }
}
