<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\laragon\www\activa_mkt/themes/pkurg-oneder/pages/blog_post.htm */
class __TwigTemplate_46082698c27393b6a08e0d3a06943c02490dc829f4651f220e26dade071a080b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("styles" => 34, "page" => 39, "framework" => 58, "scripts" => 59, "component" => 145);
        $filters = array("escape" => 9, "theme" => 16, "page" => 121);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['styles', 'page', 'framework', 'scripts', 'component'],
                ['escape', 'theme', 'page'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!doctype html>
<html lang=\"en\">
  <head>
    <title>Oneder &mdash; Website Template by Colorlib</title>
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
    
    
    <title>";
        // line 9
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, true, 9), "meta_title", [], "any", false, false, true, 9), 9, $this->source), "html", null, true);
        echo "</title>
\t<meta name=\"description\" content=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, true, 10), "meta_description", [], "any", false, false, true, 10), 10, $this->source), "html", null, true);
        echo "\">

    <link href=\"https://fonts.googleapis.com/css?family=Work+Sans:400,700,900&display=swap\" rel=\"stylesheet\">



    <link rel=\"stylesheet\" href=\"";
        // line 16
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/css/bootstrap.min.css"]);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 17
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/css/jquery-ui.css"]);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 18
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/css/owl.carousel.min.css"]);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 19
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/css/owl.theme.default.min.css"]);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 20
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/css/owl.theme.default.min.css"]);
        echo "\">

    <link rel=\"stylesheet\" href=\"";
        // line 22
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/css/jquery.fancybox.min.css"]);
        echo "\">

    <link rel=\"stylesheet\" href=\"";
        // line 24
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/css/bootstrap-datepicker.css"]);
        echo "\">

    <link rel=\"stylesheet\" href=\"";
        // line 26
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/fonts/flaticon/font/flaticon.css"]);
        echo "\">
    <link rel=\"stylesheet\" href=\"";
        // line 27
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/fonts/icomoon/style.css"]);
        echo "\">

    <link rel=\"stylesheet\" href=\"";
        // line 29
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/css/aos.css"]);
        echo "\">

    <link rel=\"stylesheet\" href=\"";
        // line 31
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/css/style.css"]);
        echo "\">
    
    <script src=\"";
        // line 33
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/js/jquery-3.3.1.min.js"]);
        echo "\"></script>
    ";
        // line 34
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('css');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('styles');
        // line 35
        echo "  </head>
  <body data-spy=\"scroll\" data-target=\".site-navbar-target\" data-offset=\"300\">
  

  ";
        // line 39
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFunction();
        // line 40
        echo "


  
  <script src=\"";
        // line 44
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/js/jquery-ui.js"]);
        echo "\"></script>
  <script src=\"";
        // line 45
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/js/popper.min.js"]);
        echo "\"></script>
  <script src=\"";
        // line 46
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/js/bootstrap.min.js"]);
        echo "\"></script>
  <script src=\"";
        // line 47
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/js/owl.carousel.min.js"]);
        echo "\"></script>
  <script src=\"";
        // line 48
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/js/jquery.countdown.min.js"]);
        echo "\"></script>
  <script src=\"";
        // line 49
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/js/jquery.easing.1.3.js"]);
        echo "\"></script>
  <script src=\"";
        // line 50
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/js/aos.js"]);
        echo "\"></script>
  <script src=\"";
        // line 51
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/js/jquery.fancybox.min.js"]);
        echo "\"></script>
  <script src=\"";
        // line 52
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/js/jquery.sticky.js"]);
        echo "\"></script>
  <script src=\"";
        // line 53
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/js/isotope.pkgd.min.js"]);
        echo "\"></script>

  
  <script src=\"";
        // line 56
        echo call_user_func_array($this->env->getFilter('theme')->getCallable(), ["assets/js/main.js"]);
        echo "\"></script>

  ";
        // line 58
        $_minify = System\Classes\CombineAssets::instance()->useMinify;
        if ($_minify) {
            echo '<script src="' . Request::getBasePath() . '/modules/system/assets/js/framework.combined-min.js"></script>'.PHP_EOL;
        }
        else {
            echo '<script src="' . Request::getBasePath() . '/modules/system/assets/js/framework.js"></script>'.PHP_EOL;
            echo '<script src="' . Request::getBasePath() . '/modules/system/assets/js/framework.extras.js"></script>'.PHP_EOL;
        }
        echo '<link rel="stylesheet" property="stylesheet" href="' . Request::getBasePath() .'/modules/system/assets/css/framework.extras'.($_minify ? '-min' : '').'.css">'.PHP_EOL;
        unset($_minify);
        // line 59
        echo "  ";
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('js');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('scripts');
        // line 60
        echo "    


<style>
.site-navbar{

background-color:#0b666e !important;

 color:white !important;
}

.nav-link{
   color:white !important;
 
   z-index:3  !important;
  
}

.site-mobile-menu, .site-navbar-target{

background-color:#0b666e !important;
}
.sticky-wrapper.is-sticky .site-navbar {
    background-color: #0B666E;
    border-bottom: 1px solid transparent;
    -webkit-box-shadow: 4px 0 20px -5px rgba(0, 0, 0, 0.1);
    box-shadow: 4px 0 20px -5px rgba(0, 0, 0, 0.1);
  
    
}

.sticky-wrapper.is-sticky .site-navbar .site-menu>li>a {
    color: white !important;
}
.sticky-wrapper.is-sticky .site-navbar .site-menu>li>a:hover,
.sticky-wrapper.is-sticky .site-navbar .site-menu>li>a.active {
    color: #bdbdbe !important;
}
</style>





  <div class=\"site-wrap\">
    <div class=\"site-mobile-menu site-navbar-target\">
        <div class=\"site-mobile-menu-header\">
            <div class=\"site-mobile-menu-close mt-3\"><span class=\"icon-close2 js-menu-toggle\"></span></div>
        </div>
        <div class=\"site-mobile-menu-body\"></div>
        
    </div>
    <header role=\"banner\" class=\"site-navbar py-4 site-navbar-target \">
        <div class=\"container\">
            <div class=\"row align-items-center\">
                <div class=\"col-6 col-xl-2\">
                    <h2 class=\"mb-0 site-logo\" ><a href=\"/\" class=\"mb-0\"><img style=\"width: 130%;\" src=\"../themes/pkurg-oneder/assets/images/LOGO ACTIVA  FINAL 2020-03.png\" ></a></h2>
                </div>
                <div class=\"col-12 col-md-10 d-none d-xl-block\">
                    <nav role=\"navigation\" class=\"site-navigation position-relative text-right\">
                        <ul class=\"site-menu main-menu js-clone-nav mr-auto d-none d-lg-block\">
                            <li><a href=\"";
        // line 121
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("main");
        echo "#home-section\" class=\"nav-link\">Inicio</a></li>
                            <li><a href=\"";
        // line 122
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("main");
        echo "#offer-section\" class=\"nav-link\">¿Qué ofrecemos?</a></li>
                            <li class=\"\"><a href=\"#services-section\" class=\"nav-link\">Servicios</a>
                            
                            </li>
                       <!--     <li><a href=\"";
        // line 126
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("main");
        echo "#about-section-2\" class=\"nav-link\">B-ACCESS</a></li>-->
                            
                            <li><a href=\"";
        // line 128
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("main");
        echo "#team-section\" class=\"nav-link\">Equipo</a></li>
                            <li><a href=\"";
        // line 129
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("main");
        echo "#blog-section\" class=\"nav-link\">Blog</a></li>
                            <li><a href=\"";
        // line 130
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("main");
        echo "#contact-section\" class=\"nav-link\">Contactanos</a></li>
                        </ul>
                    </nav>
                </div>
                <div id=\"i6iqk\" class=\"col-6 d-inline-block d-xl-none ml-md-0 py-3\"><a href=\"#\" class=\"site-menu-toggle js-menu-toggle float-right\"><span class=\"icon-menu h3\"></span></a></div>
            </div>
        </div>
    </header>

    
<div class=\"content\">
<div class=\"row\">


<div class=\"col-12 col-lg-9\">
";
        // line 145
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("blogPost"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 146
        echo "</div>


<div class=\"col-12 col-lg-3 border-left\">



<style>
.ff{
 visibility: hidden !important; 
 
 
}
 .example{
margin-top: -125%;

}
@media only screen and (max-width: 990px) {
  .example{
margin-top: -10%;
text-align:center
}
.font-size-regular{
     font-size: 20px;
     }
     .col-lg-4{
          }
    
}
</style>



<h1 class=\"text-center mt-4 pt-4\">Ultimos posts</h1>
<div class=\"mgt\">
";
        // line 181
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("blogPosts"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 182
        echo "</div>
</div>

<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>
<script>
\$(document).ready(function(){
  \$(function(){
    \$(\"div\").removeClass(\"col-lg-4 col-md-6\");
  });
});
</script>


</div>
</div>
  </body>
</html>";
    }

    public function getTemplateName()
    {
        return "C:\\laragon\\www\\activa_mkt/themes/pkurg-oneder/pages/blog_post.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  368 => 182,  364 => 181,  327 => 146,  323 => 145,  305 => 130,  301 => 129,  297 => 128,  292 => 126,  285 => 122,  281 => 121,  218 => 60,  214 => 59,  203 => 58,  198 => 56,  192 => 53,  188 => 52,  184 => 51,  180 => 50,  176 => 49,  172 => 48,  168 => 47,  164 => 46,  160 => 45,  156 => 44,  150 => 40,  148 => 39,  142 => 35,  139 => 34,  135 => 33,  130 => 31,  125 => 29,  120 => 27,  116 => 26,  111 => 24,  106 => 22,  101 => 20,  97 => 19,  93 => 18,  89 => 17,  85 => 16,  76 => 10,  72 => 9,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "C:\\laragon\\www\\activa_mkt/themes/pkurg-oneder/pages/blog_post.htm", "");
    }
}
