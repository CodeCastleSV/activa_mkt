<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\laragon\www\activa_mkt/themes/pkurg-oneder/pages/main.htm */
class __TwigTemplate_8938894ab18362a028e6cf01a04e7acd71bc28ed29d6c5e42ab8aab43db82354 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("content" => 457, "component" => 463);
        $filters = array();
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['content', 'component'],
                [],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<style type=\"text/css\">
@import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap');
@font-face {
    font-family: Quantify;
    src: url(../assets/fonts/FUENTES/Quantify.ttf);
}

@font-face {
    font-family: Magnolia Script;
    src: url(../assets/fonts/FUENTES/Magnolia Script.otff);
}

p{
    font-family: 'Montserrat', sans-serif;
}

h1,h2{
    font-family:\"Quantify\" !important;
}
    * {
        
        box-sizing: border-box;
    }
    
.principal-text{
    font-size:50px !important;
}
    
    body {
        margin-top: 0px;
        margin-right: 0px;
        margin-bottom: 0px;
        margin-left: 0px;
       
    }
    
    #i6iqk {
        position: relative;
        top: 3px;
    }
    .h2{
           font-size:0px !important;
           }
    .site-blocks-cover.overlay {

        background-image: url(\"themes/pkurg-oneder/assets/images/main_banner.png\");
        background-repeat: repeat;
        background-position: left top;
        background-attachment: scroll;
        background-size: cover;
    }

   .section-title{
        color: #0B666E;
         letter-spacing: 0.2em;
    }
    .section-title2{
         color: #0B666E;
         letter-spacing: 0.2em;
         font-size:35px !important;
    }
    .icon-color{
        color: #0b666e;
    }
    .mb{
        margin-bottom:60px;

        }
.sticky-wrapper.is-sticky .site-navbar {
    background-color: #0B666E;
    border-bottom: 1px solid transparent;
    -webkit-box-shadow: 4px 0 20px -5px rgba(0, 0, 0, 0.1);
    box-shadow: 4px 0 20px -5px rgba(0, 0, 0, 0.1);
  
    
}

.sticky-wrapper.is-sticky .site-navbar .site-menu>li>a {
    color: white !important;
}
.sticky-wrapper.is-sticky .site-navbar .site-menu>li>a:hover,
.sticky-wrapper.is-sticky .site-navbar .site-menu>li>a.active {
    color: #bdbdbe !important;
}

.responsive-text{
    font-size:50px;
}

.oc-icon-plus{
    margin-top:60px !important;
    font-size:40px !important;
}

.dropdown-menu{
    width:400px;
   
    box-shadow:none !important;
    border:solid 1px rgb(226, 226, 226) !important;
}
.presentation{
    width:400px;
   
}

@media only screen and (max-width: 770px) {
    .principal-text{
    font-size:35px !important;
}
}

@media only screen and (max-width: 600px) {
    .principal-text{
    font-size:20px !important;
}
.oc-icon-plus{
    width:100%;
    font-size:15px !important;
    margin-top:6px !important;
}

.dropdown-menu{
    width:290px;

}
.presentation{
    width:290px;
}
.section-title{
    font-size:20px;
}

.flex-row{
   flex-direction:column !important;
}
}
.tp{
margin-bottom: 100px;
}
.btn-danger{
    background-color:#FF3D22 !important;
}

.txt{
    color:#0B666E !important;
}
.btn-light{
    background-color:white !important;
}
.btn-light:hover{
    background-color:white !important;
    border:none !important;
}
</style>




<div class=\"site-wrap\">
    <div class=\"site-mobile-menu site-navbar-target\">
        <div class=\"site-mobile-menu-header\">
            <div class=\"site-mobile-menu-close mt-3\"><span class=\"icon-close2 js-menu-toggle\"></span></div>
        </div>
        <div class=\"site-mobile-menu-body\"></div>
    </div>
    <header role=\"banner\" class=\"site-navbar py-4 js-sticky-header site-navbar-target\">
        <div class=\"container\">
            <div class=\"row align-items-center\">
                <div class=\"col-6 col-xl-2\">
                    <h2 class=\"mb-0 site-logo\" ><a href=\"/\" class=\"mb-0\"><img style=\"width: 130%;\" src=\"themes/pkurg-oneder/assets/images/LOGO ACTIVA  FINAL 2020-03.png\" ></a></h2>
                </div>
                <div class=\"col-12 col-md-10 d-none d-xl-block\">
                    <nav role=\"navigation\" class=\"site-navigation position-relative text-right\">
                        <ul class=\"site-menu main-menu js-clone-nav mr-auto d-none d-lg-block\">
                            <li><a href=\"#home-section\" class=\"nav-link\">Inicio</a></li>
                            <li><a href=\"#offer-section\" class=\"nav-link\">¿Qué ofrecemos?</a></li>
                            <li class=\"\"><a href=\"#services-section\" class=\"nav-link\">Servicios</a>
                            
                            </li>
                       <!--     <li><a href=\"#about-section-2\" class=\"nav-link\">B-ACCESS</a></li>-->
                            
                            <li><a href=\"#team-section\" class=\"nav-link\">Equipo</a></li>
                            <li><a href=\"#blog-section\" class=\"nav-link\">Blog</a></li>
                            <li><a href=\"#contact-section\" class=\"nav-link\">Contactanos</a></li>
                        </ul>
                    </nav>
                </div>
                <div id=\"i6iqk\" class=\"col-6 d-inline-block d-xl-none ml-md-0 py-3\"><a href=\"#\" class=\"site-menu-toggle js-menu-toggle float-right\"><span class=\"icon-menu h3\"></span></a></div>
            </div>
        </div>
    </header>

    
    <div data-aos=\"fade\" id=\"home-section\" class=\"site-blocks-cover overlay\">
        <div class=\"container\">
            <div class=\"row align-items-center justify-content-center\">
                <div class=\"col-md-8 mt-lg-5 text-center\">
                    <h1 data-aos=\"fade-up\" class=\"text-uppercase mb-5 principal-text\">INNOVACIÓN CONSTANTE <br> DEL MUNDO DIGITAL</h1>
                    
                    <div data-aos=\"fade-up\" data-aos-delay=\"100\"><a href=\"#offer-section\" class=\"btn smoothscroll btn-danger mr-2 mb-2\">Ver más</a></div>
                </div>
            </div>
        </div><a href=\"#offer-section\" class=\"mouse smoothscroll\"><span class=\"mouse-icon\"><span class=\"mouse-wheel\"></span></span></a></div>
    
    


<section id=\"offer-section\" class=\"mb\"></section>


    
    <div id=\"\" class=\"site-section cta-big-image\">
        <div class=\"container\">
            <div class=\"row mb-4\">
                <div data-aos=\"fade\" class=\"col-12 text-center\">
                    <h2 class=\"section-title mb-3\">¿Qué ofrecemos?</h2>
                </div>
            </div>
            <div class=\"row\">
                <div data-aos=\"fade-up\" data-aos-delay=\"\" class=\"col-lg-6 mb-5\">
                    <figure ><img style=\"width:95%;\" src=\"themes/pkurg-oneder/assets/images/Ofrecemos.jpg\" alt=\"Image\" class=\"img-fluid\" /></figure>
                </div>


                <div data-aos=\"fade-up\" data-aos-delay=\"100\" class=\"col-lg-6 ml-auto\">

                    <div class=\"\">
                <div data-aos=\"fade-up \" data-aos-delay=\"100 \" class=\"col-12 \">
                    <div class=\"unit-4 row \">
                        <div class=\"unit-4-icon col-3 col-md-2 mr-4 \"><span class=\"icon-color   \" style=\" font-size: 50px;\"> <img style=\"width:95%;\"  src=\"themes/pkurg-oneder/assets/images/ic_soluciones_personalizadas.png\" alt=\"\"> </span ></div>
                        <div class=\"col-8 col-md-9\">
                            <h3>Soluciones personalizadas y efectivas</h3>
                            <p>Desarrollo acorde a las necesidades de los clientes que brinden eficiencia y competitividad en sus procesos.</p>

                        </div>
                    </div>
                </div>
                <div id=\"design-section\">

                    <div data-aos=\"fade-up \" data-aos-delay=\"100 \" class=\"col-12 \">
                        <div class=\"unit-4 row\">
                            <div class=\"unit-4-icon col-3 col-md-2 mr-4\"><span class=\" icon-color\" style=\" font-size: 50px;\"> <img style=\"width:95%;\"  src=\"themes/pkurg-oneder/assets/images/ic_soporte.png\" alt=\"\"></span></div>
                            <div class=\"col-8 col-md-9\">
                                <h3>Soporte</h3>
                                <p class=\"txy\">Acompañamiento en los desarrollos, con la finalidad de convertirnos en sus aliados.</p>

                            </div>
                        </div>
                    </div>
                </div>
                <div id=\"branding-section\">
                    <div data-aos=\"fade-up\" data-aos-delay=\"200\" class=\"col-12 \" >
                        <div class=\"unit-4 row\">
                            <div class=\"unit-4-icon col-3 col-md-2 mr-4\"><span class=\" \"><img style=\"width:100%;\"  src=\"themes/pkurg-oneder/assets/images/ic_transferencia_conocimiento.png\" alt=\"\"></span></div>
                            
                            <div class=\"col-8 col-md-9\">
                                <h3>Transferencia de conocimiento</h3>
                                <p>Facilitar a nuestros clientes capacitaciones y material de apoyo continuo.</p>

                            </div>
                        </div>
                    </div>
                </div>
              
                <div data-aos=\"fade-up\" data-aos-delay=\"100\"><a href=\"#form-section\" class=\"btn smoothscroll btn-danger mr-2 mb-2\"  style=\" font-size:18px; margin-top:25px;\"> Más información </a></div>
                   
            </div>
                </div>
            </div>
        </div>
    </div>









<section id=\"services-section\" class=\"mb\"></section>

<br>
    

    <section id=\"\" class=\"site-section border-bottom bg-light mb-5\">
        <div class=\"container\">
            <div class=\"row mb-5  \">
                <div data-aos=\"fade\" class=\" col-12 col-md-5 \">
                    <h2 class=\"section-title2 mb-3\">NUESTROS</h2>
                    <h3 class=\"  section-title mb-3\">SERVICIOS</h3>
                </div>
            
            

            <div class=\"col-12 col-md-7 border\">
                <div data-aos=\"fade-up\" class=\"col-12\" >
                    <div class=\"unit-4 row\">
                        <div class=\"unit-4-icon col-3 col-md-2 mr-4 mt-4 \"><span class=\"\" style=\" font-size: 50px;\"><img style=\"width:100%;\"  src=\"themes/pkurg-oneder/assets/images/ic_soluciones_efectivas.png\" alt=\"\"></span></div>
                        <div class=\"col-8 col-md-9\">
                            <h3>Desarrollos digitales</h3>
                            <p>Aplicaciones móviles, desarrollos web y desarrollo a la medida de las necesidades.</p>

                        </div>
                    </div>
                </div>
                <div id=\"design-section\">


                    <div data-aos=\"fade-up \" data-aos-delay=\"100 \" class=\"col-12 \">
                        <div class=\"unit-4 row\">
                            <div class=\"unit-4-icon col-3 col-md-2 mr-4 mt-4\"><span class=\"\" style=\" font-size: 50px;\"><img style=\"width:100%;\"  src=\"themes/pkurg-oneder/assets/images/ic_diseno_crea_contenido.png\" alt=\"\"></span></div>
                            <div class=\"col-8 col-md-9\">
                                <h3>Diseño y creación de contenido digital</h3>
                                <p>Optimizamos campaña online en todas las plataformas, para ayudarle a atraer seguidores convertirlos en clientes.</p>

                            </div>
                        </div>
                    </div>
                </div>
                <div id=\"branding-section\">
                    <div data-aos=\"fade-up\" data-aos-delay=\"200\" class=\"col-12 \" >
                        <div class=\"unit-4 row\">
                            <div class=\"unit-4-icon col-3 col-md-2 mr-4 mt-4\"><span class=\"\" style=\" font-size: 50px;\"><img style=\"width:100%;\"  src=\"themes/pkurg-oneder/assets/images/ic_branding.png\" alt=\"\"></span></div>
                            <div class=\"col-8 col-md-9\">
                                <h3>Branding</h3>
                                <p>Creadores de identidad visual con personalidad, haciendo que la comunicación visual de su negocio sea coherente, atractiva y profesional.</p>

                            </div>
                        </div>
                    </div>
                </div>
                <div id=\"analysis-section\">
                    <div data-aos=\"fade-up\" data-aos-delay=\"\" class=\"col-12 \" >
                        <div class=\"unit-4 row\">
                            <div class=\"unit-4-icon col-3 col-md-2 mr-4 mt-4\"><span class=\"\" style=\" font-size: 50px;\"><img style=\"width:100%;\"  src=\"themes/pkurg-oneder/assets/images/ic_analisis_entorno.png\" alt=\"\"></span></div>
                            <div class=\"col-8 col-md-9\">
                                <h3>Analisis de entorno</h3>
                                <p>Analizamos su sector de mercado, su posicionamiento de marca, el de su competencia,<br> tanto en un entorno offline como online.</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div></div>
   
    </section>




<!--

<section id=\"about-section-2\" class=\"mb\"></section>


<br>
<section id=\"\" class=\"site-section \">
        <div class=\"container-fluid\">
            <div class=\"row mb-5\">
                <div data-aos=\"fade-down\" data-aos-delay=\"\" class=\"col-lg-6 ml-auto mb-5 d-flex justify-content-end  order-2 order-lg-2\"><img src=\"themes/pkurg-oneder/assets/images/Captura.PNG\" alt=\"Image\" class=\"img-fluid rounded\" /></div>
                <div data-aos=\"fade\" class=\"col-lg-5 order-1 order-lg-1 \">
                    <div class=\"row\">
                        <div data-aos=\"fade-up\" data-aos-delay=\"\" class=\"col-md-12 mb-md-5 mb-0 col-lg-12\">
                            <div class=\"unit-4 \">
                              <h2 class=\"section-title \" style=\"margin-top:70px;\">HERRAMIENTAS PARA LA ADMINISTRACIÓN</h2>  
                                <div>
                                
                        <p>Software diseñado pensando en los pequeños negocios, que desean modernizar su
organización y dar un paso más adelante, en la era digital.</p>

<div class=\"d-flex flex-row bd-highlight col-12\">



<div class=\"dropdown\">
    <a    class=\"btn  btn-light oc-icon-plus txt font-weight-bold\">B-</a>
</div>


<div class=\"dropdown\">
 <a href=\"#\" data-toggle=\"dropdown\"   class=\"btn btn-light oc-icon-plus\"><strong class=\"text-danger\"> A </strong></a>

    <ul class=\"dropdown-menu\" role=\"menu\">
        <p role=\"presentation\">B-Accesible Se puede implementar en cualquier dispositivo, con mínimos requerimientos
técnicos. </p>
    </ul>
      </div> 

      <div class=\"dropdown\">
    <a href=\"#\" data-toggle=\"dropdown\"   class=\"btn btn-light oc-icon-plus txt font-weight-bold\">C</a>

    <ul class=\"dropdown-menu\" role=\"menu\" data-dropdown-title=\"Add something small\">
        <li role=\"presentation\">B-Conveniente Se adapta a cualquier tipo de negocio, ferretería, farmacia, restaurante, tienda de
ropa, etc. </li>
    </ul>
</div>

<div class=\"dropdown\">
    <a href=\"#\" data-toggle=\"dropdown\"   class=\"btn btn-light oc-icon-plus txt font-weight-bold\">C</a>

    <ul class=\"dropdown-menu\" role=\"menu\" data-dropdown-title=\"Add something small\">
        <li role=\"presentation\">B-Cercano Fácil de administrar, brindamos además soporte inmediato.</li>
    </ul>
</div>

<div class=\"dropdown\">
    <a href=\"#\" data-toggle=\"dropdown\"   class=\"btn btn-light oc-icon-plus txt font-weight-bold\">E</a>

    <ul class=\"dropdown-menu\" role=\"menu\" data-dropdown-title=\"Add something small\">
        <li role=\"presentation\">B-Económico Precio accesible, con opciones para adquisición de licencia de uso mensual. </li>
    </ul>
</div>


<div class=\"dropdown\">
    <a href=\"#\" data-toggle=\"dropdown\"   class=\"btn btn-light oc-icon-plus txt font-weight-bold\">S</a>

    <ul class=\"dropdown-menu\" role=\"menu\" data-dropdown-title=\"Add something small\">
        <li role=\"presentation\">B-Sencillo Ambiente intuitivo, para los clientes que compren en la tienda virtual. Compra fácil
y rápida. </li>
    </ul>
</div>



<div class=\"dropdown\">
    <a href=\"#\" data-toggle=\"dropdown\"   class=\"btn btn-light oc-icon-plus txt font-weight-bold\">S</a>

    <ul class=\"dropdown-menu\" role=\"menu\" data-dropdown-title=\"Add something small\">
        <li role=\"presentation\">B-Soluciones Soluciona las necesidades básicas de todo negocio, como análisis de inventario,
cuentas por pagar, entre otros.</li>
    </ul>
</div>



</div>
     
                                </div>
                            </div>
                        </div>
                   <div data-aos=\"fade-up\" data-aos-delay=\"100\"><a href=\"#form-section\" class=\"btn smoothscroll btn-danger mr-2 mb-2\"  style=\" width:120px; font-size:18px; margin:25px;\">  Cotizar </a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
-->

<section id=\"team-section\" class=\"mb\">

</section>
 <div class=\"mb\"> </div>

";
        // line 457
        $context['__cms_content_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->contentFunction("carousel"        , $context['__cms_content_params']        );
        unset($context['__cms_content_params']);
        // line 458
        echo "

<section class=\"tp\" id=\"blog-section\">
</section>

";
        // line 463
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("blogPosts"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 464
        echo "
          <!--      <div id=\"b-access-section\">
                    <div data-aos=\"fade-up\" data-aos-delay=\"100\" class=\"col-12 \" >
                        <div class=\"unit-4\">
                            <div class=\"unit-4-icon mr-4\"><span class=\"text-primary flaticon-smartphone\"></span></div>
                            <div>
                                <h3>B-ACCESS</h3>
                                <p>Herramientas para la administración de su negocio + venta en línea en un solo lugar. <br><br> Software diseñado pensando en los pequeños negocios, que desean modernizar su organización y dar un paso más adelante, en la
                                    era digital.
                                </p>
                                <ul>
                                    <li style=\"margin-bottom: 6px;\"><strong>B-Accesible</strong> Se puede implementar en cualquier dispositivo, con mínimos requerimientos técnicos.
                                    </li>
                                    <li style=\"margin-bottom: 6px;\"><strong>B-Conveniente</strong>Se adapta a cualquier tipo de negocio, ferretería, farmacia, restaurante, tienda de ropa, etc.</li>
                                    <li style=\"margin-bottom: 6px;\"><strong>B-Cercano</strong>Fácil de administrar, brindamos además soporte inmediato.</li>
                                    <li style=\"margin-bottom: 6px;\"><strong>B-Económico</strong>Precio accesible, con opciones para adquisición de licencia de uso mensual.</li>
                                    <li style=\"margin-bottom: 6px;\"><strong>B-Sencillo</strong>Ambiente intuitivo, para los clientes que compren en la tienda virtual. Compra fácil y rápida.</li>
                                    <li style=\"margin-bottom: 6px;\"><strong>B-Soluciones</strong>Soluciona las necesidades básicas de todo negocio, como análisis de inventario, cuentas por pagar, entre otros.</li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

-->
   


    ";
        // line 496
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("contactform"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 497
        echo "


    
    <footer class=\"site-footer\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-9\">
                    <div class=\"row\">
                        <div class=\"col-md-3\">
                            <h2 class=\"footer-heading mb-4\"><img width=\"100%\" src=\"themes/pkurg-oneder/assets/images/LOGO ACTIVA  FINAL 2020-01.png\" alt=\"\"></h2>
                            <p>Dinamica moderna, Activa Marketing combina la experiencia de su equipo de trabajo con más de 20 años en el área de comunicaciones y el mercadeo, con la innovación constante del mundo digital.</p>
                        </div>
                        <div class=\"col-md-3 \">
                              
                        </div>
                               <div class=\"col-md-3 ml-auto\">
                            <h2 class=\"footer-heading mb-4\">Quick Links</h2>
                            <ul class=\"list-unstyled\">
                             <li><a href=\"#home-section\" class=\"smoothscroll\">Inicio</a></li>
                                <li><a href=\"#offer-section\" class=\"smoothscroll\">¿Que ofrecemos?</a></li>
                                <li><a href=\"#services-section\" class=\"smoothscroll\">Servicios</a></li>
                               <!-- <li><a href=\"#about-section-2\" class=\"smoothscroll\">B-ACCESS</a></li>-->
                                <li><a href=\"#team-section\" class=\"smoothscroll\">Equipo</a></li>
                                 <li><a href=\"#blog-section\" class=\"smoothscroll\">Blog</a></li>
                                  <li><a href=\"#contact-section\" class=\"smoothscroll\">Contactanos</a></li>
                            </ul>
                        </div>
                        <div class=\"col-md-3\">
                            <h2 class=\"footer-heading mb-4\">Follow Us</h2><a href=\"https://www.facebook.com/ACTIVAMkt-103823568149146\" target=\"_blank\" class=\"pl-0 pr-3\"><span class=\"icon-facebook\"></span></a>
                            
                            <a href=\"https://www.instagram.com/activamkt\" target=\"_blank\" class=\"pl-3 pr-3\"><span class=\"icon-instagram\"></span></a>

                            <a href=\"https://wa.me/68282413\" target=\"_blank\" class=\"pl-3 pr-3\"><span class=\"icon-phone\"></span></a>
                           
                        </div>
                    </div>
                </div>
                <div class=\"col-md-3\">
                    <h2 class=\"footer-heading mb-4\">Subscribete </h2>
                  ";
        // line 537
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("contactform2"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 538
        echo "                </div>
                    </div>
                </div>
                
            </div>
        
        </div>
    </footer>
</div>



<!-- .site-wrap -->";
    }

    public function getTemplateName()
    {
        return "C:\\laragon\\www\\activa_mkt/themes/pkurg-oneder/pages/main.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  619 => 538,  615 => 537,  573 => 497,  569 => 496,  535 => 464,  531 => 463,  524 => 458,  520 => 457,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "C:\\laragon\\www\\activa_mkt/themes/pkurg-oneder/pages/main.htm", "");
    }
}
